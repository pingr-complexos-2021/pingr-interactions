const express = require("express");
const router = express.Router();
const { ObjectID } = require("bson");

module.exports = function ({ db }) {
  router.post("/likes", async (req, res) => {
    const userId = res.locals.user._id;

    // Validate user
    let user = await db.collection("users").findOne(
      { _id: userId }
    );

    if (!user) {
      return res.status(404).json({
        error: "User not found"
      });
    }

    // Validate ping
    let pingId = req.body.pingId;
    let ping = await db.collection("pings").findOne(
      { _id: ObjectID(pingId) }
    );

    if (!ping) {
      return res.status(404).json({
        error: "Ping not found"
      });
    }

    // Validate user did not like this before
    let like = await db.collection("likes").findOne(
      {
        pingId: pingId,
        userId: String(user._id)
      }
    );

    if (like) {
      return res.status(422).json({
        error: "Ping already liked by user"
      });
    }

    const likeAttributes = {
      createdAt: Date.now(),
      pingId: pingId,
      userId: String(user._id),
    }
    await db.collection("likes").insertOne(likeAttributes);

    return res.status(201).json(likeAttributes);
  });

  router.delete("/likes/:id", async (req, res) => {
    // Validate if like is from this user
    const likeId = req.params.id;
    const userId = String(res.locals.user._id);

    let like = await db.collection("likes").findOne(
      {
        _id: ObjectID(likeId),
        userId: userId
      }
    );

    if (!like) {
      return res.status(401).json({
        error: "Unauthorized unlike"
      });
    }

    await db.collection("likes").deleteOne({ _id: ObjectID(likeId) });
    return res.status(200).json({});
  });

  router.get("/likes", async (req, res) => {
    const userId = String(res.locals.user._id);
    let pingId = req.query.pingId;
    let likes = await db.collection("likes").find(
      { pingId: pingId }
    ).toArray();

    let myLikeId = null;
    if (userId) {
      let myLikes = likes.filter(like => like.userId === userId);
      if (myLikes.length > 0) {
        myLikeId = myLikes[0].id;
      }
    }

    return res.status(200).json({
      count: likes.length,
      likeId: myLikeId,
      likes: likes
    });
  });

  return router;
};
