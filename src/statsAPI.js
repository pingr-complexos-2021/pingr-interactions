const express = require("express");
const router = express.Router();

module.exports = function ({ db }) {
  router.get("/stats/:pingId", async (req, res) => {
    const { pingId } = req.params;

    const pongs = await db.collection("pongs").count({ pingId });
    const likes = await db.collection("likes").count({ pingId });
    const replies = await db.collection("replies").count({ parentId: pingId });

    res.status(200).json({
      counts: {
        likes,
        pongs,
        replies
      }
    });
  });

  return router;
};
