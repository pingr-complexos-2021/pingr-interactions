const express = require("express");
const router = express.Router();
const { ObjectID } = require("bson");

module.exports = function ({ db, stanConn }) {
  const pongsCollection = db.collection("pongs");
  const usersCollection = db.collection("users");
  const pingsCollection = db.collection("pings");

  const validatePing = async (req, res, next) => {
    let pingId = req.body.pingId || req.query.pingId;

    const ping = await pingsCollection.findOne({ _id: ObjectID(pingId) });
    if (!ping) {
      return res.status(409).json({ error: "There is no ping matching this ping id" });
    }

    next();
  };

  router.post("/pongs", validatePing, async (req, res) => {
    const user = res.locals.user;
    const pingId = req.body.pingId;
    const pong = {
      userId: user._id,
      pingId: pingId
    };

    const pongOnDatabase = await pongsCollection.findOne(pong);
    if (pongOnDatabase) {
      return res.status(409).json({ error: "This ping was already PONGED!" });
    }

    pong.createdAt = Date.now();
    stanConn.publish("pongs", JSON.stringify({
      eventType: "PongCreated",
      entityAggregate: pong
    }));

    return res.status(200).json({});
  });

  router.delete("/pong/:pongId", async (req, res) => {
    const user = res.locals.user;
    const pongId = req.params.pongId;

    const pong = await pongsCollection.findOne({ _id: ObjectID(pongId) });
    if (!pong) {
      return res.status(409).json({ error: "There is no pong matching this pong id" });
    }

    if (pong.userId !== String(user._id)) {
      return res.status(403).json({ error: "This user can't delete this pong" });
    }

   stanConn.publish("pongs", JSON.stringify({
     eventType: "PongDeleted",
     entityId: pongId
   }));

    return res.status(200).json({ message: "Pong successfully deleted" });
  });

  router.get("/pongs", validatePing, async (req, res) => {
    let pingId, pongCount, userPong, pongs;

    const user = res.locals.user;
    const userId = user.id;

    pingId = req.query.pingId;
  
    pongs = await pongsCollection.find({pingId: pingId}).toArray();

    userPong = pongs.find(pong => {
        return (pong.userId == userId);
    })

    pongCount = pongs.length;

    let userPongId = userPong?._id;

    let pongOwners = [];

    for (pong of pongs) {
      let thisPongId = pong._id;
      let thisUserId = pong.userId;

      let thisUser = await usersCollection.findOne({ _id: ObjectID(thisUserId) });
      pongOwners.push({pongId: thisPongId, username: thisUser.username});
    }

    const pongsResponse = {count: pongCount, myPongId: userPongId, pongs: pongOwners};

    res.status(200).json(pongsResponse);
    return;
  });

  return router;
}