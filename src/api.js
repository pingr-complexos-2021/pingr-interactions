const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");

const likeAPI = require("./likeAPI");
const pongAPI = require("./pongApi");
const replyAPI = require("./replyAPI");
const statsAPI = require("./statsAPI");
const { ObjectID } = require("bson");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();
  const db = mongoClient.db("interactions");

  const checkAuthorization = async (req, res, next) => {
    const token = req.get("Authorization").split(" ")[1];
    if (!token) {
      res.status(403).json({
        error: 'Missing authorization token'
      });
      return;
    }

    const decodedToken = jwt.verify(token, secret).userId;
    const user = await db.collection("users").findOne({
      _id: ObjectID(decodedToken)
    });
    if (!user) {
      res.status(403).json({
        message: 'Unauthorized'
      });
      return;
    }

    res.locals.user = user;
    next();
  }

  api.use(express.json());
  api.use(cors(corsOptions));
  api.use(checkAuthorization);

  api.use(likeAPI({ db }));
  api.use(pongAPI({ db, stanConn }));
  api.use(replyAPI({ db }));
  api.use(statsAPI({ db }));

  return api;
};
