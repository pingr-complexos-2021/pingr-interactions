const stan = require("node-nats-streaming");
const { ObjectID } = require("bson");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });
  const db = mongoClient.db("interactions");
  const pongsCollection = db.collection("pongs");

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    const opts = conn.subscriptionOptions().setStartWithLastReceived();

    const pongsSubscription = conn.subscribe("pongs", opts);
    pongsSubscription.on("message", async (msg) => {
      const event = JSON.parse(msg.getData());

      if (event.eventType === "PongCreated") {
        await pongsCollection.insertOne(event.entityAggregate);
      } else if (event.eventType === "PongDeleted") {
        const id = event.entityId;
        await pongsCollection.deleteOne({ _id: ObjectID(id) });
      }
    });

    const pingSubscription = conn.subscribe("pings", opts);
    pingSubscription.on("message", async (msg) => {
      const event = JSON.parse(msg.getData());
      const _id = event.entityId;
      const pingAttributes = {
        ...event.entityAggregate,
        _id
      };
      const reply = {
        parentId: pingAttributes.parentId,
        userId: pingAttributes.userId
      };
      
      if (event.eventType === "PingCreated") {
        db.collection("pings").insertOne(pingAttributes);
        
        if (reply.parentId) {
          db.collection("replies").insertOne(reply);
        }
      } else if (event.eventType === "PingUpdated") {
        db.collection("pings").updateOne({ _id }, pingAttributes);
      }
    });

    const userSubscription = conn.subscribe("users", opts);
    userSubscription.on("message", (msg) => {
      const event = JSON.parse(msg.getData());
      const _id = ObjectID(event.entityId);
      const userAttributes = {
        ...event.entityAggregate,
        _id,
      };

      if (event.eventType === "UserCreated") {
        db.collection("users").insertOne(userAttributes)
      } else if (event.eventType === "PingUpdated") {
        db.collection("users").updateOne({ _id }, userAttributes);
      }
    });
  });

  return conn;
};
