
const express = require("express");
const router = express.Router();

module.exports = function ({ db }) {
  router.get("/replies", async (req, res) => {
    const { userId, parentId } = req.query;
    const shouldList = req.query.shouldList === "false"
      ? false
      : true;

    const findOpts = {};
    if (parentId) {
      findOpts['parentId'] = parentId;
    }
    if (userId) {
      findOpts['userId'] = userId;
    }

    const replies = await db.collection("replies").find(findOpts).toArray();
    const count = replies.length;

    const response = shouldList
      ? { count, replies }
      : { count, replies: [] };

    res.status(200).json(response);
  });

  return router;
};
