const request = require("supertest");
const jwt = require("jsonwebtoken");
const API = require("../src/api");

describe("replyAPI", () => {
  const secret = "712OculosEscuros!#";
  const corsOptions = { origin: "*" };

  describe("GET /replies", () => {
    it("returns an array of replies by default", async () => {
      const mockColl = {
        find: jest.fn(() => ({
          toArray: () => [{ _id: 'mock-reply' }]
        })),
        findOne: jest.fn(() => ({ _id: 'mock-user' })),
      };
  
      const mockDb = { collection: () => mockColl };
      const mongoClient = { db: () => mockDb };
      const stanConn = jest.fn();
      const api = API(corsOptions, { mongoClient, stanConn, secret });
  
      const token = jwt.sign({ userId: 1 }, secret);
      const response = await request(api)
        .get("/replies")
        .set("Authorization", `Bearer ${token}`);
  
      expect(response.status).toBe(200);
      expect(response.body.count).toBe(1);
      expect(response.body.replies.length).toBe(1);
    });

    it("returns an empty array of replies when shouldList is false", async () => {
      const mockColl = {
        find: jest.fn(() => ({
          toArray: () => [{ _id: 'mock-reply' }]
        })),
        findOne: jest.fn(() => ({ _id: 'mock-user' })),
      };
  
      const mockDb = { collection: () => mockColl };
      const mongoClient = { db: () => mockDb };
      const stanConn = jest.fn();
      const api = API(corsOptions, { mongoClient, stanConn, secret });
  
      const token = jwt.sign({ userId: 1 }, secret);
      const response = await request(api)
        .get("/replies?shouldList=false")
        .set("Authorization", `Bearer ${token}`);
  
      expect(response.status).toBe(200);
      expect(response.body.count).toBe(1);
      expect(response.body.replies.length).toBe(0);
    });
  });
});
