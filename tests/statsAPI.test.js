const request = require("supertest");
const jwt = require("jsonwebtoken");
const API = require("../src/api");

describe("statsAPI", () => {
  const secret = "712OculosEscuros!#";
  const corsOptions = { origin: "*" };

  describe("GET /stats/:pingId", () => {
    it("returns an object with count of pongs, likes and replies", async () => {
      const mockColl = {
        count: jest.fn(() => 0),
        findOne: jest.fn(() => ({ _id: 'mock-user' })),
      };
  
      const mockDb = { collection: () => mockColl };
      const mongoClient = { db: () => mockDb };
      const stanConn = jest.fn();
      const api = API(corsOptions, { mongoClient, stanConn, secret });
  
      const token = jwt.sign({ userId: 1 }, secret);
      const response = await request(api)
        .get("/stats/mock-ping-id")
        .set("Authorization", `Bearer ${token}`);
  
      expect(response.status).toBe(200);
      expect(response.body.counts.likes).toBe(0);
      expect(response.body.counts.pongs).toBe(0);
      expect(response.body.counts.replies).toBe(0);
    });
  });
});
