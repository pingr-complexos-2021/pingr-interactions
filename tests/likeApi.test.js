// External libraries
const request = require("supertest");
const jwt = require("jsonwebtoken");

// API module
const API = require("../src/api");

describe("The project", () => {
  let api, mockDb, mongoClient, stanConn;
  const corsOptions = { origin: "*" };
  const secret = "712OculosEscuros!#";

  it("can create like", async () => {
    let mockColl = {
      findOne: jest.fn(query => query.id ? query : null),
      insertOne: jest.fn(query => query)
    };

    mockDb = { collection: () => mockColl };
    mongoClient = { db: () => mockDb };
    stanConn = { publish: jest.fn() };
    api = API(corsOptions, { mongoClient, stanConn, secret });

    const token = jwt.sign({ id: 1 }, secret);
    const response = await request(api)
      .post("/likes")
      .send({ pingId: 1 })
      .set("Authorization", `Bearer ${token}`);

    expect(response.status).toBe(201);
    expect(response.body.payload).toBeDefined();
    expect(response.body.payload.pingId).toBe(1);
    expect(response.body.payload.userId).toBe(1);
  });

  it("cannot like twice", async () => {
    let mockColl = {
      findOne: jest.fn(query => query),
      insertOne: jest.fn(query => query)
    };

    mockDb = { collection: () => mockColl };
    mongoClient = { db: () => mockDb };
    stanConn = { publish: jest.fn() };
    api = API(corsOptions, { mongoClient, stanConn, secret });

    const token = jwt.sign({ id: 1 }, secret);
    const response = await request(api)
      .post("/likes")
      .send({ pingId: 1 })
      .set("Authorization", `Bearer ${token}`);

    expect(response.status).toBe(422);
    expect(response.body.payload).toBeUndefined();
  });

  it("can delete like", async () => {
    let mockColl = {
      deleteOne: jest.fn(),
      findOne: jest.fn(query => query)
    };

    mockDb = { collection: () => mockColl };
    mongoClient = { db: () => mockDb };
    stanConn = { publish: jest.fn() };
    api = API(corsOptions, { mongoClient, stanConn, secret });

    const token = jwt.sign({ id: 1 }, secret);
    const response = await request(api)
      .delete("/likes/1")
      .set("Authorization", `Bearer ${token}`);

    expect(response.status).toBe(200);
  });

  it("can get likes", async () => {
    let mockColl = {
      find: jest.fn(() => [])
    };

    mockDb = { collection: () => mockColl };
    mongoClient = { db: () => mockDb };
    stanConn = { publish: jest.fn() };
    api = API(corsOptions, { mongoClient, stanConn, secret });

    const token = jwt.sign({ id: 1 }, secret);
    const response = await request(api)
      .get("/likes?ping_id=1")
      .set("Authorization", `Bearer ${token}`);

    expect(response.status).toBe(200);
    expect(response.body.count).toBe(0);
    expect(response.body.likeId).toBe(null);
    expect(response.body.likes.length).toBe(0);
  });
});
